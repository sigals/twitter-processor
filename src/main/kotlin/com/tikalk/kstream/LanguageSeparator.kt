package com.tikalk.kstream

import com.tikalk.kstream.twitter.data.Tweet
import com.tikalk.kstream.twitter.serde.TweetSerde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.*
import java.util.*

fun main() {
    val config = Properties()
    config[StreamsConfig.APPLICATION_ID_CONFIG] = "split-by-lang-ks"
    config[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
    config[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.String().javaClass.name
    config[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = TweetSerde::class.java

    val builder = StreamsBuilder()
    val stream: KStream<String, Tweet> =
        builder.stream(Constants.TWEETS, Consumed.with(Serdes.String(), TweetSerde()))

    stream
        .peek( MyAction("First"))
        .selectKey(MyMapper())
        .peek( MyAction("Second"))
        .to(Constants.LANGUAGES_TWEETS, Produced.with(Serdes.String(), TweetSerde()))

    val streams = KafkaStreams(builder.build(), config)
    streams.start()
}

class MyAction(
    private val prefix: String
) : ForeachAction<String, Tweet> {
    override fun apply(key: String, value: Tweet) {
        println("$prefix: key $key, value's text ${value.text!!.substring(0, 10)}")
    }

}

class MyMapper : KeyValueMapper<String, Tweet, String> {
    override fun apply(key: String, value: Tweet): String {
        return value.language ?: "na"
    }

}
