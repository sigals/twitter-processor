package com.tikalk.kstream

import com.tikalk.kstream.twitter.TweetProducer
import com.tikalk.kstream.twitter.TweetQueries

// Get tweets by a query criteria and push to a Kafka topic
fun main() {

    val tp = TweetProducer()
    tp.produceTweets(Constants.TWEETS, TweetQueries.getQueries())
}
