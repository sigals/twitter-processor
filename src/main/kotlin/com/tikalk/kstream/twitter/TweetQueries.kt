package com.tikalk.kstream.twitter

import java.util.*

object TweetQueries {
    private val queries: MutableList<String> = ArrayList()
    fun getQueries(): List<String> {
        return queries
    }

    init {
        queries.add("q=kafka")
        queries.add("q=beirut")
        queries.add("q=covid")
        queries.add("q=balfur")
        queries.add("q=aws")
        queries.add("q=confluent")
        queries.add("q=rabbitmq")
    }
}
