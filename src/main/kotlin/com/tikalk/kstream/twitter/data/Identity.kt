package com.tikalk.kstream.twitter.data

interface Identity<T> {
    fun getKey(): T
}
