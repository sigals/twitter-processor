package com.tikalk.kstream.twitter.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import twitter4j.Status
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class Tweet() : Identity<String> {

    @JsonProperty
    var id: String = ""
    var status: Status? = null
    var decoded: Boolean = false
    var inResponseTo: Long? = null
    var favoriteCount: Int? = null
    var retweetCount = 0
    var text: String? = null
    var createdAt: Date? = null
    var language: String? = null
    var source: String? = null
    var userName: String? = null

    constructor(id: String, status: Status) : this(){
        this.id = id
        favoriteCount = status.favoriteCount
        this.retweetCount = status.retweetCount
        text = status.text
        createdAt = status.createdAt
        language = status.lang
        source = status.source
        inResponseTo = status.inReplyToStatusId
        decoded = false
        userName = status.user.name
    }
    override fun getKey() = id
}
