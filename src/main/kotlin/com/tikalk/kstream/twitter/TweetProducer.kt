package com.tikalk.kstream.twitter

import com.tikalk.kstream.twitter.data.Tweet
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import twitter4j.Query
import twitter4j.Status
import twitter4j.Twitter
import twitter4j.TwitterFactory
import twitter4j.conf.ConfigurationBuilder
import java.util.*


class TweetProducer {
    private var producer: KafkaProducer<String, Tweet>
    private lateinit var properties: Properties
    private var twitter: Twitter

    init {
        val tf: TwitterFactory = initFactory()
        this.twitter = tf.instance
        initProps()
        producer = KafkaProducer(properties)
    }

    private fun initFactory(): TwitterFactory {
        val cb = ConfigurationBuilder()
        cb.setDebugEnabled(true)
            .setOAuthConsumerKey("syLHmMpyVbhkMEq4ua0xqTEEW")
            .setOAuthConsumerSecret("rWbfUOtAT3osmF1ACcJ4gs4AO3NuLPop9PNvdtbv4CZHW3Ccij")
            .setOAuthAccessToken("4537889248-WFl9VLppfc30BxKfjF9BY8y7kVXPuCRp4tSIHGD")
            .setOAuthAccessTokenSecret("N4lWvSFfO2hMdNK1NpzgT6q3GUEd6ai28zz938QxYs5kX")
        return TwitterFactory(cb.build())
    }

    private fun initProps() {
        properties = Properties()

        properties["bootstrap.servers"] = "localhost:9092"
        properties["acks"] = "1"
        properties["retries"] = 0
        properties["batch.size"] = 512
        properties["linger.ms"] = 1
        properties["key.serializer"] = "org.apache.kafka.common.serialization.StringSerializer"
        properties["value.serializer"] = "com.tikalk.kstream.twitter.serde.TweetSerializer"
    }

    fun produceTweets(topic: String, queries: List<String>) {
        for (query in queries) {
            for (status in searchTweets(query)) {
                produce(Tweet(status.id.toString(), status), topic)
            }
        }
    }

    private fun searchTweets(queryString: String?): List<Status> {
        val query = Query(queryString)
        query.count = 100
        val result = twitter.search(query)
        return result.tweets
    }

    private fun produce(record: Tweet, topic: String) {
        producer.send(ProducerRecord(topic, record.getKey(), record))
        try {
            Thread.sleep(50)
        } catch (e: InterruptedException) {
            throw RuntimeException(e.message)
        }
    }
}
