package com.tikalk.kstream.twitter.serde

import com.fasterxml.jackson.databind.ObjectMapper
import com.tikalk.kstream.twitter.data.Tweet
import org.apache.kafka.common.serialization.Deserializer
import org.slf4j.LoggerFactory

class TweetDeserializer : Deserializer<Tweet> {
    private val log = LoggerFactory.getLogger(TweetDeserializer::class.java)
    private val objectMapper = ObjectMapper()

    override fun deserialize(topic: String?, data: ByteArray?): Tweet? {
        var retVal: Tweet? = null
        try {
            if (data != null) {
                retVal = objectMapper.readValue(data, Tweet::class.java)
            }
        } catch (e: Exception) {
            log.error("failed to deserialize:", e)
            throw RuntimeException(e)
        }
        return retVal
    }

    override fun close() {}

}
