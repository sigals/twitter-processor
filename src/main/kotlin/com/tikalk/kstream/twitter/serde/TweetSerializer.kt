package com.tikalk.kstream.twitter.serde

import com.fasterxml.jackson.databind.ObjectMapper
import com.tikalk.kstream.twitter.data.Tweet
import org.apache.kafka.common.serialization.Serializer
import org.slf4j.LoggerFactory

/**
 * Serializes a Tweet to a byte array representation of a JSon
 */
class TweetSerializer : Serializer<Tweet> {
    private val objectMapper = ObjectMapper()

    override fun configure(configs: Map<String?, *>?, isKey: Boolean) {}

    override fun serialize(topic: String, data: Tweet): ByteArray {
        val retVal: ByteArray
        retVal = try {
            objectMapper.writeValueAsBytes(data)
        } catch (e: Exception) {
            log.error("Failed to serialize", e)
            throw RuntimeException(e)
        }
        return retVal
    }

    override fun close() {}

    companion object {
        private val log = LoggerFactory.getLogger(TweetSerializer::class.java)
    }
}
