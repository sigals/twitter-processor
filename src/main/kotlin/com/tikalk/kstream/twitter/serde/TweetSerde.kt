package com.tikalk.kstream.twitter.serde

import com.tikalk.kstream.twitter.data.Tweet
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serializer

class TweetSerde : Serde<Tweet> {
    override fun configure(configs: Map<String?, *>?, isKey: Boolean) {}

    override fun close() {}

    override fun serializer(): Serializer<Tweet> {
        return TweetSerializer()
    }

    override fun deserializer(): Deserializer<Tweet> {
        return TweetDeserializer()
    }
}
